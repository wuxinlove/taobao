const moment = require('moment');
const config = require('../common/config.json')
const sea = require('../common/sea')
var crypto = require('crypto');

class utils {
    changeToString(time) {
        var TimeInfo = new Date(time)
        var year = TimeInfo.getFullYear();
        var mon = TimeInfo.getMonth() + 1
        var day = TimeInfo.getDate()
        if (day < 10) {
            day = "0" + mon
        }

        if (mon < 10) {
            mon = "0" + mon
        }
        return year + "-" + mon + "-" + day
    }

    changeToStringDate(time) {
        return moment(time).format('YYYY-MM-DD HH:mm:ss');
    }

    formatCook(obj) {
        var stringVal = ""
        for (var i in obj) {
            stringVal += i + "=" + obj[i] + ";"
        }
        stringVal = stringVal.substring(0, stringVal.length - 1)
        return stringVal

    }

    changeToObj(obj) {
        try {
            if (typeof obj == "string") {
                obj = JSON.parse(obj)
            }
            return obj
        }catch (e){
          return {}
        }

    }

    chechSing(signData) {
        try {
            console.log("加密的数据是", signData)
            let signDataString = sea.decrypt(sea.key, sea.vi, signData);
            if (typeof signDataString === 'string') signDataString = JSON.parse(signDataString);
            if (signDataString.code != 200) return ({code: 500, msg: "加密错误1"});
            const signDecodedInfo = JSON.parse(signDataString.decoded);
            let sign = signDecodedInfo.sign;
            let MacId = signDecodedInfo.MacId;

            let sign_ = sea.decrypt(sea.key, sea.vi, sign)
            if (sign_.code != 200) return ({code: 500, msg: "加密错误2"})
            const sign_decoded = JSON.parse(sign_.decoded);
            if (sign_decoded.MacId != MacId + "jinyu") return ({code: 500, msg: "macId 错误"})
            if (parseInt(new Date().getTime()) - JSON.parse(sign_decoded.time) >= 1000 * 60 * 2) return ({
                code: 500,
                msg: "时间错误"
            });
            return {code: 200, signDecodedInfo: signDecodedInfo}
        } catch (e) {
            console.error("加密错误：", e)
            return ({code: 500, msg: "错误"})
        }
    }


    async  sentMsgInfo(PhoneNumbers, TemplateCode) {
        try {
            const mySignName = '淘饭饭';
            const msgRes = await smsClient.sendSMS({
                PhoneNumbers,
                SignName: mySignName,
                TemplateCode,
            });
            // console.log("44444444444", msgRes);
            let res = {code: 500, info: '验证码发送失败'};
            if (msgRes.Code === 'OK') res = {code: 200, info: '验证码发送成功'};
            return res;
        } catch (e) {
            return {code: 500, info: e};
        }
    }


     MakeStr(obj){
        let newkey = Object.keys(obj).sort();
         // let newkey   =  [ 'client_id', 'goods_id_list',  'type' ,'timestamp' ]
        var retString = "";
         for(var i =0 ;i <newkey.length;i++){
             var value
             let key = newkey[i]
             if(key == "goods_id_list"){
                 value  = `[${obj[key]}]`
             }else {
                 value = obj[key]
             }
             retString += (key + value)
         }
         // retString = retString.substring(0,retString.length -1)
        return retString;//返回排好序的新对象
    }

    PDDSign(str){
        let signStr = config.client_secret + str +  config.client_secret
        // let signStr = "78172s8ds9a921j9qqwda12312w1w21211client_id1data_typeXMLorder_status1page1page_size10timestamp1480411125typepdd.order.number.list.gettestSecret"
        console.log("signStr",signStr)
        let sign = this.Md5Sgin(signStr)
        return  sign.toUpperCase()
    }

    Md5Sgin(str){
        let Md5key = str;
        let md5=crypto.createHash("md5");
        md5.update(Md5key);
        let strMac = md5.digest('hex');
        return strMac
    }

}

module.exports = new utils();



