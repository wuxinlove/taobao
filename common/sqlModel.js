/**
 * Created by linwei on 2017/6/22.
 */

let ConnectionInfo = require('./connection');
(async ()=>{
    try{
        let connection =await  ConnectionInfo.init();

        exports.connectionLocalhot =  connection.driver;
        exports.app_account = connection.define("app_account", {
            id: {type: 'integer', key: true, serial: true},
            account: {type: 'text'},  //账号
            mm: {type: 'text'},  //淘宝客mm
            passWd: {type: 'text'},  //密码
            phone: {type: 'text'},  //电话
            MacId: {type: 'text'},  //MacId
            aliveTime: {type: 'text'},  //vip时间
            time: {type: 'text'},  //VIP参考时间
            createTime: {type: 'text'},  //注册时间
            updateTime: {type: 'text'},  //订单有变化时间
            status: {type: 'integer'},  //状态 0：正常 -1 非vip
            shareTimes: {type: 'integer',default: 0},  //分享次数
            shareId:{type: 'text',default: ""}, //分享者id
            cutNum:{type: 'text',default: ""}, //可以截图次数
            signUpTime:{type: 'integer',default: 0}, //签到时间
            money:{type: 'integer',default: ""}, //余额
            moneyBake:{type: 'integer',default: ""}, //返利金额
            needBackMoney:{type: 'integer',default: ""}, //待返利金额
            icon:{type: 'text',default: ""}, //头像
            nick:{type: 'text',default: ""}, //昵称
            sex:{type: 'text',default: ""}, //性别
            borth:{type: 'text',default: ""}, //生日
            aliAccount:{type: 'text',default: ""}, //支付宝账e号
            realName:{type: 'text',default: ""}, //真实姓名
            shareMoney:{type: 'text',default: ""}, //分享的账号获得的钱
            blackUser:{type: 'integer',default: ""}, //黑名单中
        });

        exports.taobaoitem = connection.define("taobaoitem", {
            phone: {type: 'text'},  //推广者电话
            category_id: {type: 'text'},  //叶子类目id
            commission_rate: {type: 'text'},  //佣金比率  1550表示15.5%
            coupon_share_url: {type: 'text'},  //券二合一页面链接
            shop_title: {type: 'text'},  //
            shop_dsr: {type: 'text'},  //店铺dsr评分
            user_type: {type: 'text'},  //卖家类型，0表示集市，1表示商城
            reserve_price: {type: 'text'},  //商品一口价格
            zk_final_price: {type: 'text'},  //商品折扣价格
            title: {type: 'text'},  //商品标题
            nick: {type: 'text'},  //
            seller_id: {type: 'text'},  //卖家id
            volume: {type: 'text'},  //30天销量
            pict_url: {type: 'text'},  //商品主图
            item_url: {type: 'text'},  //商品地址
            coupon_total_count: {type: 'text'},  //优惠券总量
            coupon_info: {type: 'text'},  //优惠券面额
            num_iid: {type: 'text', key: true, serial: true},  // 宝贝id
            coupon_remain_count: {type: 'text'},  //优惠券剩余量
            coupon_start_time: {type: 'text'},  //
            coupon_end_time: {type: 'text'},  //
            item_description: {type: 'text'},  //
            url: {type: 'text'},  //商品淘客链接
            tk_total_commi: {type: 'text'},  //月支出佣金
            coupon_id: {type: 'text'},  //优惠券id
            provcity: {type: 'text'},  //宝贝所在地
            oetime: {type: 'text'},  //拼团：结束时间
            ostime: {type: 'text'},  //拼团：开始时间
            jdd_num	: {type: 'text'},  //拼团：几人团
            jdd_price	: {type: 'text'},  //拼团：拼成价，单位元
            // mm	: {type: 'text'},  //拼团：拼成价，单位元
        });

        exports.global = connection.define("global", {
            id: {type: 'integer', key: true, serial: true},  // ID
            type:{type: 'integer'},  //类型
            msg:{type: 'text'},  //描述
            value: {type: 'integer'},  // 值
        });


        exports.tokenUrl = connection.define("tokenurl", {
            id: {type: 'integer', key: true, serial: true},  // ID
            url:{type: 'text'},  //
        });

        exports.order = connection.define("order", {
            id: {type: 'integer', key: true, serial: true},  // ID
            userId:{type: 'integer'},  //用户信息
            auctionId:{type: 'integer'},  //商品id
            skuId:{type: 'integer'},  //商品类型id
            taobaoTradeParentId:{type: 'text'},  //订单id
            taobaoTradeSubId:{type: 'text'},  //子订单id
            shopId: {type: 'text'},  // 商店id
            shopName: {type: 'text'},  // 商店名字
            payPrice: {type: 'text'},  // 商品单价
            auctionNum: {type: 'text'},  // 商品数量
            payStatus: {type: 'text'},  // 支付状态
            realPayFee: {type: 'text'},  // 付款金额
            discountAndSubsidyToString: {type: 'text'},  // 佣金比率
            finalDiscountToString: {type: 'text'},  //
            totalAlipayFeeString: {type: 'text'},  // 佣金金额
            toMyuser: {type: 'text'},  // 返回给玩家的金额 没返回0，已经返回了具体的金额
        });


        exports.code =  connection.define("code",{
            name: {type: 'text'},  //名字
            code: {type: 'text'},  //复活码
            time: {type: 'text'},  //过期时间
            timeStart: {type: 'text'},  //过期时间
        });
        exports.question =  connection.define("question",{
            id: {type: 'integer', key: true,serial:true},
            answer: {type: 'text'},  //题目
            question: {type: 'text'},  //答案
            channel: {type: 'text'},  //channel
        });

        exports.account =  connection.define("account",{
            id: {type: 'integer', key: true,serial:true},
            account: {type: 'text'},  //账号
            passWord: {type: 'text'},  //密码
            time: {type: 'text'},  //开始时间
            aliveTime: {type: 'text'},  //存活时间
            status: {type: 'text'},  //存活时间
        });

        exports.timeList = connection.define("timeList", {
            id: {type: 'integer', key: true, serial: true},
            status: {type: 'integer'},
            hour: {type: 'text'},  //题目
            min: {type: 'text'},  //答案
            applicationName: {type: 'text'},  //答案
            rewardAmount: {type: 'text'},  //答案
            packageName: {type: 'integer'},  //状态 0：正常 -1 删除
        });

        exports.questionLister = connection.define("questionLister", {
            id: {type: 'integer', key: true, serial: true},
            question: {type: 'text'},  //题目
            answerList: {type: 'text'},  //答案集合
            answer: {type: 'text'},  //答案
            channel: {type: 'text'},  //channel
        });

        exports.app_phone_code = connection.define("app_phone_code", {
            id: {type: 'integer', key: true, serial: true},
            phone: {type: 'text'},  //电话
            code: {type: 'text'},  //code码
            time: {type: 'text'},  //时间
        });

        exports.itemList = connection.define("itemList", {
            id: {type: 'integer', key: true, serial: true},
            name: {type: 'text'},  //商品名称
            time: {type: 'text'},  //时间
            status: {type: 'integer'},  //状态
            totalMoney: {type: 'integer'},  //显示价格
            realMoney: {type: 'integer'},  //实际价格
            type: {type: 'integer'},  //类型 1：vip时间，2 ：金币
            value: {type: 'integer'},  //类型 1：vip时间，2 ：金币
        });

        exports.appOrder = connection.define("app_order", {
            id: {type: 'integer', key: true, serial: true},
            accountId: {type: 'text'},  //商品名称
            orderId: {type: 'text'},  //商品名称
            itemId: {type: 'text'},  //时间
            money: {type: 'integer'},  //显示价格
            status: {type: 'integer'},  //实际价格
        });

        exports.channel = connection.define("channel", {
            id: {type: 'integer', key: true, serial: true},
            channel: {type: 'text'},  //渠道
            status: {type: 'integer'},  //状态
        });

        exports.roomInfo = connection.define("roomInfo", {
            id: {type: 'integer', key: true, serial: true},
            type: {type: 'integer'},  //房间类型
            status: {type: 'integer'},  //状态
            golden: {type: 'integer'},  //需要的金币
            questionNum: {type: 'integer'},  //题目数量
            msg: {type: 'text'},  //描述
        });

        exports.transfer = connection.define("transfer", {
            id: {type: 'integer', key: true, serial: true},
            channel: {type: 'text'},  //渠道
            money: {type: 'integer'},  //金额
            PayAccount: {type: 'text'},  //支付账号
            AccountID: {type: 'text'},  //玩家账号
            orderId: {type: 'text'},  //订单号
            status: {type: 'integer'},  //状态
            time: {type: 'integer'},  //时间
            aliAccount: {type: 'integer'},  //提现账号
        });

        exports.signUp = connection.define("signUp", {
            id: {type: 'integer', key: true, serial: true},
            isVip: {type: 'integer'},  //是否vip
            value: {type: 'integer'},  //奖励值
            type: {type: 'integer'},  // 奖励类型
        });


        //
        exports.needGetUserId = connection.define("needgetuserId", {
            id: {type: 'text', key: true},  // ID
            adzoneid: {type: 'text'},  // adzoneid
        });

       // EXadzoneid
        exports.EXadzoneid = connection.define("exadzoneids", {
            adzoneid: {type: 'text'},  // adzoneid
            channelName: {type: 'text', key: true},  // channelName
        });

        exports.needGetItemId = connection.define("itemid", {
            id: {type: 'integer', key: true},  // ID
        });

        exports.searchMsg = connection.define("searchmsg", {
            id: {type: 'integer', key: true, serial: true},
            msg: {type: 'integer'},  //描述
        });

        // 淘宝客订单
        exports.taobaokeOrder = connection.define("taobaokeorde", {
            id: {type: 'integer', key: true, serial: true},  // ID
            userId:{type: 'text'},  //用户信息
            taobaoTradeParentId:{type: 'text'},  //订单id
            taobaoTradeSubId:{type: 'text'},  //子订单id
            shopId: {type: 'text'},  // 商店id
            shopName: {type: 'text'},  // 商店名字
            payPrice: {type: 'text'},  // 商品单价
            auctionNum: {type: 'text'},  // 商品数量
            payStatus: {type: 'text'},  // 支付状态
            payStatusCode: {type: 'text'},  // 支付状态
            // realPayFee: {type: 'text'},  // 付款金额
            discountAndSubsidyToString: {type: 'text'},  // 佣金比率
            discountAndSubsidyToStringReal: {type: 'text'},  // 佣金比率（实际）
            finalDiscountToString: {type: 'text'},  // 佣金金额
            totalAlipayFeeString: {type: 'text'},  // 付款金额
            toMyuser: {type: 'text'},  // 返回给玩家的金额 没返回0，已经返回了具体的金额
            msg: {type: 'text'},  // 商品描述
            icon: {type: 'text'},  // 商品图片
            url: {type: 'text'},  // 商品链接
            shareUrl: {type: 'text'},  // 商品推广链接
            createTime: {type: 'text'},  // 创建时间
            payMoney: {type: 'text'},  // 结算金额
            mayPay: {type: 'text'},  // 预估收入
            itemId: {type: 'text'},  // 商品id
            orederTime: {type: 'text'},  // 开始计算返利时间
            share: {type: 'text'},  // 是否是返利物品
            feeString: {type: 'text'},  // 佣金金额
            updateTime: {type: 'text'},  // 更新时间
            updateRealTime: {type: 'text'},  // 自动更新时间
            earningTime: {type: 'text'},  // 结算时间
            rate: {type: 'text'},  // 入库时的百分比
        });

        exports.clientTable = connection.define("clienttable", {
            id: {type: 'integer', key: true, serial: true},
            key: {type: 'text'},  //描述
            value: {type: 'text'},  //描述
        });

        exports.needSearchOrder = connection.define("needsearchorder", {
            id: {type: 'integer', key: true,serial: true},
            time:{type: 'text'},  // 时间
            value:{type: 'text'},  // 时间
        });

        exports.shareInfo = connection.define("shareinfo", {
            id: {type: 'integer', key: true,serial: true},
            masterId:{type: 'text'},  // 分享者电话
            shareId:{type: 'text'},  // 被分享电话
            time:{type:"text"},// 分享时间
        });

        // 黑名单
        //exports.blackList  = connection.define("blacklist", {
        //    id: {type: 'integer', key: true,serial: true},
        //    account:{type: 'text'},  // 账号
        //    MacId:{type: 'text'},  // MacId
        //});


        connection.sync();
    }
    catch(err){
        console.log(err);
    }
})();

