/**
 * Created by linwei on 2017/6/22.
 */
var orm = require("orm");
let config = require('../common/config.json')
let dbConfit = config.dbConfit;

class MysqlContent {
    constructor() {
        orm.settings.set("connection.pool ", true);
        orm.settings.set("connection.debug", true);
        orm.settings.set("connection.reconnect", true);
        //允许多条查询
        console.log("dbConfit", dbConfit);
    }

    async init (){
      console.log("initial")
      try {
          return  await orm.connectAsync(dbConfit)
        }catch(e){
          console.error(e)
        }
    }
}

module.exports = new MysqlContent();

