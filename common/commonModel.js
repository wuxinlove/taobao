/**
 * Created by linwei on 2017/6/22.
 */

let ConnectionInfo = require('./connection');
(async ()=>{
  try{
      let connection = await  ConnectionInfo.init();
      let connectionLocalhot =  connection.driver;
      exports.connectionLocalhot = connectionLocalhot;
  }
  catch(err){
    console.log(err);
  }
})();

