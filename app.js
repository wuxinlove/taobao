const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
require("./common/connection")

const index = require('./routes/index');
const init = require('./common/Init');


const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// 全局变量
global.cookie2 = "1c3503affe5e257a1e550bce5a15d8a1";
global.token = "eee5ef11efe50";
global.rate = 0.4;
global.payEndTime = 7
global.payEndTime = 20
global.t = "8cf46ccc6af20858335666766f388a25";
global.cookies = "";



app.use('/', index);


setTimeout(function () {
    init.initGlobal()
    init.getAdzoneId()
    // init.GetOrderVerTime()
    init.GetOrderVerTimeDown()
    // init.getLogin()
    init.updateOrder()
},2000)


app.use(function(req, res, next) {
  var err = new Error('Not Found');
  console.error("err is Not Found");
  err.status = 404;
  next("Not Found");
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
